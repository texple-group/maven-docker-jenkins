FROM tomcat

COPY ./webapp/target/webapp.war /usr/local/tomcat/webapps/

RUN ls -l /usr/local/tomcat/webapps/

CMD ["/usr/local/tomcat/bin/catalina.sh", "run"]
